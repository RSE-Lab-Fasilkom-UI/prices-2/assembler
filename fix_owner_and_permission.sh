#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

echo "--- Directory: ${DIR} ---"
echo "* Set default owner of whole files and directories"
sudo chown -R www-data:rse ${DIR}
echo "* Fix permission of index HTML files"
sudo chmod 664 ${DIR}/index.nginx-debian.html
echo "* Fix permission of permission fixer script"
sudo chmod 770 ${DIR}/fix_owner_and_permission.sh

echo "--- Directory: ${DIR}/engine ---"
echo "* Set default permission of all files and directories"
sudo find ${DIR}/engine -type f -exec chmod 660 {} \;
echo "* Fix permission of executable files in ${DIR}/engine/abs"
sudo chmod 770 ${DIR}/engine/abs/**/*.bat ${DIR}/engine/abs/**/*.sh
echo "* Fix permission of executable files in ${DIR}/engine/admin"
sudo chmod 770 ${DIR}/engine/admin/app.py ${DIR}/engine/admin/gev.py
echo "* Fix permission of executable files in ${DIR}/engine/cli"
sudo chmod 770 ${DIR}/engine/cli/runner.py ${DIR}/engine/cli/setup.py
echo "* Fix permission of executable files in ${DIR}/engine/ifml"
sudo chmod 770 ${DIR}/engine/ifml/__init__.py

echo "--- Directory: ${DIR}/monitoring ---"
echo "* Set default permission of all files and directories"
sudo chmod -R 771 ${DIR}/monitoring
sudo find ${DIR}/monitoring -type f -exec chmod 660 {} \;
echo "* Fix permission of executable files"
sudo chmod 770 ${DIR}/monitoring/manage.py

echo "--- Directory: ${DIR}/html ---"
echo "* Set default permission of all files and directories"
sudo chmod -R 771 ${DIR}/html
sudo find ${DIR}/html -type f -exec chmod 660 {} \;
echo "* Fix owner of secret files and directories"
sudo chown root:rse ${DIR}/html/artisan ${DIR}/html/*.md ${DIR}/html/composer.json ${DIR}/html/composer.lock ${DIR}/html/docker-compose.yml ${DIR}/html/Dockerfile ${DIR}/html/.editorconfig ${DIR}/html/.env ${DIR}/html/.env.example ${DIR}/html/.gitattributes ${DIR}/html/.gitignore ${DIR}/html/.htaccess ${DIR}/html/LICENSE ${DIR}/html/phpunit.xml ${DIR}/html/.travis.yml
sudo chown -R root:rse ${DIR}/html/.git ${DIR}/html/docker
echo "* Fix permission of files and directories"
sudo chmod 770 ${DIR}/html/artisan
sudo chmod 775 ${DIR}/html/index.php ${DIR}/html/server.php
sudo chmod 664 ${DIR}/html/.env

echo "--- Directory: ${DIR}/productlines/* ---"
echo "* Set default permission of all files and directories"
sudo chmod -R 771 ${DIR}/productlines
sudo find ${DIR}/productlines -type f -exec chmod 660 {} \;
echo "* Fix owner of secret files and directories"
sudo chown root:rse ${DIR}/productlines/*/artisan ${DIR}/productlines/*/*.md ${DIR}/productlines/*/composer.json ${DIR}/productlines/*/composer.lock ${DIR}/productlines/*/docker-compose.yml ${DIR}/productlines/*/Dockerfile ${DIR}/productlines/*/.editorconfig ${DIR}/productlines/*/.env ${DIR}/productlines/*/.env.example ${DIR}/productlines/*/.gitattributes ${DIR}/productlines/*/.gitignore ${DIR}/productlines/*/.htaccess ${DIR}/productlines/*/LICENSE ${DIR}/productlines/*/phpunit.xml ${DIR}/productlines/*/.travis.yml
sudo chown -R root:rse ${DIR}/productlines/*/.git ${DIR}/productlines/*/docker
echo "* Fix permission of files and directories"
sudo chmod 770 ${DIR}/productlines/*/artisan
sudo chmod 775 ${DIR}/productlines/*/index.php ${DIR}/productlines/*/server.php
sudo chmod 664 ${DIR}/productlines/*/.env

echo "--- Directory: ${DIR}/products/* ---"
echo "* Fix owner of all files and directories"
sudo chown -R www-data:www-data ${DIR}/products
echo "* Fix owner of shared directories"
sudo chown www-data:rse ${DIR}/products
sudo chown www-data:rse ${DIR}/products/nginx ${DIR}/products/temp
sudo chown -R www-data:rse ${DIR}/products/node ${DIR}/products/log_backup
echo "* Fix permission of shared directories"
sudo chmod -R 771 ${DIR}/products/node
echo "* Fix permission of product files and directories"
sudo find ${DIR}/products -type f | grep -e build.bat -e build.sh -e run.bat -e run.sh -e app.py -e gev.py | sudo xargs chmod 771
