#!/bin/bash
############################################
# SPLELIVE DEBIAN-BASED SERVER SETUP WRAPPER
############################################

############################################
# BEFORE YOU RUN THIS SCRIPT:
# If you are using other non-Debian Linux distros (e.g. Arch), change apt-get to your package manager
# Place this script alongside all of the components inside /var/www
############################################

############################################
# MANUAL CONFIGURATIONS TO DO AFTER USING THIS SCRIPT:
# # Set Sudoers (read CLI's README.md Initialization Step 1 number 5)
#   /usr/sbin/visudo
#
# # Change SSH port to 22422
#   - Change /etc/ssh/sshd_config
#   - Change security group in your cloud provider, add inbound rule, to accept tcp - port 22422 - from anywhere.
#
# # Setup nginx.conf (read CLI's README.md Initialization Step 1 number 3)
#
# # Setup wildcard HTTPS certificate using Certbot.
#   - To install Cerbot: sudo apt-get install certbot
#   - You can see tutorials on the internet. Wildcard HTTPS certificate involves DNS-based verification.
############################################

# Create new RSE group. To edit files inside /var/www
addgroup rse &

# Update System Packages
/usr/bin/apt update
/usr/bin/apt upgrade

# Install dependencies
/usr/bin/curl -sL https://deb.nodesource.com/setup_10.x | bash -
/usr/bin/apt install nodejs git nginx-full certbot python3.7 python3-pip ant
/usr/bin/npm install -g yarn
/usr/sbin/update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.7 2
/usr/sbin/update-alternatives --config python3
python3.7 get-pip.py

# Install database (pick one by uncommenting lines that have two hashes (##)):
# PostgreSQL:
/usr/bin/apt install postgresql postgresql-contrib
/bin/systemctl enable postgresql
/bin/systemctl start postgresql
# MySQL: (https://www.fosstechnix.com/install-mysql5-on-ubuntu/)
## /usr/bin/wget http://repo.mysql.com/mysql-apt-config_0.8.9-1_all.deb
## /usr/bin/dpkg -i mysql-apt-config_0.8.9-1_all.deb
## /usr/bin/apt install mysql-server
## /usr/bin/mysql_secure_installation

# Initialize permission for /var/www
/bin/chown -R www-data:rse /var/www

# Pull latest engine commits
/usr/bin/git -C /var/www/engine/admin/ pull origin fb-url:fb-url
/usr/bin/git -C /var/www/engine/admin/ checkout fb-url
/usr/bin/git -C /var/www/engine/abs/ pull origin affan/dev:affan/dev
/usr/bin/git -C /var/www/engine/abs/ checkout affan/dev
/usr/bin/git -C /var/www/engine/ifml/ pull origin fix-atob:fix-atob
/usr/bin/git -C /var/www/engine/ifml/ checkout fix-atob
/usr/bin/git -C /var/www/engine/cli/ pull origin post-launch:post-launch
/usr/bin/git -C /var/www/engine/cli/ checkout post-launch
/usr/bin/git -C /var/www/monitoring/ pull origin master:master
/usr/bin/git -C /var/www/monitoring/ checkout master

# Pull latest site commits
/usr/bin/git -C /var/www/html/ pull origin feature/ui:feature/ui
/usr/bin/git -C /var/www/html/ checkout feature/ui
/usr/bin/git -C /var/www/productlines/aisco/ pull origin feature/ui:feature/ui
/usr/bin/git -C /var/www/productlines/aisco/ checkout feature/ui

# Install CLI requirements
/usr/bin/python3 -m pip install -r /var/www/engine/cli/requirements.txt

# Fix permissions for /var/www
/bin/chmod +x /var/www/fix_owner_and_permission.sh
/var/www/fix_owner_and_permission.sh

